# BrainDot Coding Style

This document is inspired by the [Linux Kernel Style](https://www.kernel.org/doc/html/v4.10/process/coding-style.html) and various other styles, including Java and Kotlin styles.

You may use the following language-specific guides:
- [C, C++](STYLE_C.md)
- [Kotlin](STYLE_Kt.md)
- [Java](STYLE_Java.md)

If a language-specific guide conflicts with this one, follow it. You should still read this one though, as language-specific guides will not repeat what is already specified here.

## Indentation

Indent your code with tabulations, not spaces; a width of 4 spaces is recommended. If you need to align things, use spaces. For example:

```c
if (something) {
    return "First line, "
         + "second line.";
}
```

In the above example, the third line uses a single 'tab' character for indentation, followed by spaces for alignment.

For languages based on brackets, if you decide to not use them after a conditional or a loop (`if`, `for`, etc.) you should put the code in its own line:

```c
if (something)
    execute();
```

Do not leave whitespace at the end of lines.

## Long lines

Your lines should be under 80 characters long. An 81- or 82-characters long line is fine if you need to, but do not go above 100 characters as it makes code harder to read.

If you need to split your lines, prefer putting the symbol on the beginning of the next line such that the start of the line is aligned (if your language supports it):

```c
return "This is a test. "
     + "With multiple lines. "
     + "Where the start is aligned";
```

## Brackets

For languages using brackets, the opening bracket should always be at the previous line.

```c
if (something) {
    execute();
    executeAsWell();
}
```

The closing bracket is on its own line expect for composed constructs (eg. `do ... while` and `if ... else`):

```c
do {
    if (something) {
        execute();
        executeAsWell();
    } else {
        executeOther();
    }
} while (test);
```

In constructs with a single line, do not use the brackets:

```c
if (test)
    execute();
```

If your `if` has brackets, the corresponding `else` (and every `else if`) should have brackets as well, even if they are single lines:

```c
if (test) {
    execute();
    executeAsWell();
} else {
    executeThird();
}
```

## Spaces

Put a space between keywords and parenthesis, but not after functions:

```c
if (something)
    execute();
```

Do not put spaces inside parenthesis:

```c
execute(test);    // Good
execute( test );  // Bad
```

Use spaces around binary operators (`+`, `*`, `-`, etc) but not after unary operators (`!`, `-`, etc):

```c
int a = 2 * 3 + (-2);
```

Do not put spaces around object accessors; such as `.` or `->` in C.

Put a space after comments:

```c
// This is a good comment
//This is a bad comment
```

## Naming

Naming conventions are very different from one language to the other, and will likely be overridden in their language file.

You should name variables clearly, but without taking unneeded space. It is fine naming variables `tmp` or similar names, it isn't to call them `temporaryCounter` which doesn't had any information and is harder to read. However if you can have a more precise variable name, it is better to.

Anything public should have a descriptive name (no `tmp` or `foo` there).

As an example, if you have a function that counts a number of users, it should be named `countUsers()` or similar, but the counter variable used inside can be called `counter` or even `n`.

Do not use single letters as variable names, unless they are already explicit enough (`i` for the iterator in a `for` loop, `n` for a counter or a number of iterations...) and only used in a small scope (inside a function). Never name parameters/arguments with a single letter.

## Functions

Functions should not be longer than around 20 lines: otherwise, you are probably doing something wrong. The only allowed case is if the function is trivial (eg. a long list of initializations), but even then we recommend using helper functions.

If a function has more than around 10 variables, it should probably be simplified.

No function should reach 6 or 7 levels of indentation. After 4 levels you should probably worry and create helper functions.

If two functions have the same name (overloading), the two functions should do the same thing, with only small differences. Reading the documentation of one should make it perfectly clear what the other does (eg. only use overloads for default parameters, to accept different types…).

## Exiting of functions

In general, every return statement should be at the same indentation level.

In trivial functions, returning from within a construct (a loop) to short-circuit it is allowed.

## Documentation

See the individual languages guides. If none applies to your language, using whatever is most accepted by the community of your language.

Anything accessible from outside of the current context (the current file) should be documented.

## Ternary choices

For languages that support the ternary operator (`... ? ... : ...`), either enclose each clause in parentheses:

```c
return (i == 0) ? (a + 2) : (b + 3);
```

or put each clause on its own line:

```c
return i == 0
     ? a + 2
     : b + 3;
```
