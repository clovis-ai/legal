# BrainDot Coding Style

This directory consists of a general explanation of our coding style, and different files that explain the specifics for each language.

You can find the general coding style [here](STYLE.md). If you find conflicting information, the language-specific
 coding style takes precedence.

You can also find our [Git](STYLE_Git.md) commit style.

A configuration file for JetBrains IDEs can be found [here](BrainDot_Style.xml). Note that using this file doesn't
 prevent you from reading the documents in this repository, as they don't take care of everything.
