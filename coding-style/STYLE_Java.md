# BrainDot Coding Style: Java

This document inherits from [the main guide](STYLE.md). Please ensure you have read it.

This style takes inspiration from the [Oracle conventions](https://www.oracle.com/technetwork/java/codeconventions-150003.pdf) and the [Google conventions](https://google.github.io/styleguide/javaguide.html).

## Source files

Each file contains a single public class or interface. You can put related private classes or interfaces at the end of the file.

Declarations should appear in this order:
- Fields
- Constructors
- Regular methods
- Getters/Setters
- Overridden methods (equals, hashCode…)
- Static fields
- Static methods

Overloads (of methods or constructors) should always be consecutive with no code whatsoever between them.

## Naming

Classes, interfaces and enums should be follow `PascalCase`. Use nouns (`Passenger`, `Tool`). The name of an interface can be an adjective if it represents a behavior (`Readable`, `Comparable`).

Variables, fields and methods should follow `camelCase`. Methods are typically verbs (`send`).

Constants should follow `SCREAMING_SNAKE_CASE` if they represent a marker value (a setting, a constant like pi, a key in a Map…), and should follow `camelCase` if they represent a normal value that happens not to change.

Enumeration instances should follow `SCREAMING_SNAKE_CASE` for marker values, and `PascalCase` if they represent a set of objects (eg. if they have a behavior). All instances of the same enumeration should follow the same rules.

Do NOT use automatic prefixes or suffixes (`ISomething`, `SomethingImpl`, `_example`, `myExample`, etc).

The name of packages is your website in the opposite order followed by a name (eg. `fr.braindot.projectname`). Use exactly this case.

## Variable declarations

Do not shadow variables (having a variable in an inner scope with the same name as one in an outer scope). The only exception is for method/constructor parameters which have the same name as an attribute:
```java
class Example {
    private int example;

    void test() {
        int other;
        if (true) {
            int other; // Bad
        }
    }

    public void setExample(int example) { // Good
        this.example = example;
    }
}
```

Put the array brackets after the type:
```java
int[] a; // Good
int a[]; // Bad
```
Unless performance is a major concern, always use collections instead of arrays.

Never rely on default values for any type:
```java
class Example {
    private final int example1 = 0; // Good
    private final int example2;     // Good: initialized in all constructors
    private int example3;     // Bad: not initialized at all

    Example() {
        example2 = 0;
    }
}
```

Only declare multiple variables on the same line if they represent the same concept:
```java
int x, y;             // Good
int i, somethingElse; // Bad
```

## Getters and setters

Only add getters and setters if they are meaningful. In most cases, internal state is modified by methods, and the user doesn't need access to the state at all.

When possible, create immutable classes.

## Modeling a missing value

When trying to model a missing value in a private context (inside a method, as a private field…) use `null`.

When trying to model a missing value in the return of a method, always use `Optional` ([rationale](https://stackoverflow.com/a/54619104/5666171)).

`null` is preferred for parameter types, but `Optional` is allowed as well. Parameters that shouldn't be `null` should always be checked, for example with `Objects#requireNonNull(Object, String)`.

```java
class Example {
    private final String nullable = null;
    public Optional<String> getNullable() {...}
}
```

The only exception is when writing code exclusively for Kotlin. In that case, use the `@Nullable` and `@NotNull` annotations ([List of allowed annotations](https://github.com/JetBrains/kotlin/blob/master/core/compiler.common.jvm/src/org/jetbrains/kotlin/load/java/JvmAnnotationNames.kt), [Kotlin documentation](https://kotlinlang.org/docs/reference/java-interop.html)).

## No useless code

In general, do not write useless code, unless you're absolutely sure it helps readability. As an example, do not specify the type in the diamond operator if the compiler can infer it:
```java
List<String> l = new ArrayList<>();       // Good
List<String> l = new ArrayList<String>(); // Bad
```

## Type parameters

Always use the most specific type you can think of for a type parameter. Only use `Object` when you have a good reason to.

Always put specify the type parameter:
```java
List l;         // Bad
List<Object> l; // Allowed in specific cases
List<String> l; // Good
```

## Switch

Fallthrough explicitly (with a comment) in a `case` that has at least one instruction:
```java
switch (5) {
case -1:  // No need for a comment
case 0:
    System.out.println("yes");
    // Fallthrough (<- mandatory comment)
case 1:
    System.out.println("no");
    break;
default:
    System.out.println("other");
}
```

## Annotations

The `@Override` annotation is mandatory everywhere the compiler allows it without warnings or errors (only exception is when the parent is `@Deprecated`).

When creating an interface to be used as a lambda (SAM interfaces), annotate it with `@FunctionalInterface`.

Place annotations on classes and functions on the previous line.

Place annotations on parameters on the same line (`@NotNull String other`).

Place annotations on a variable declaration on the previous line. If there is a single annotation, it is allowed to put it at the beginning of the line.

Place annotations between the Javadoc and the declaration.

## Modifiers

Modifiers appear in this order:
```java
public protected private abstract default static final transient volatile synchronized native strictfp
```

## Exceptions

It is very rarely correct to ignore a caught exception. If there truly is nothing to do, add a comment in the `catch` clause explaining why.

## Documentation

Javadoc is mandatory on everything that is `public` or `protected`.

Javadoc shouldn't be a simple rephrasing of a method name. It should explain what it does, what parameters are allowed or not, which error cases exist, etc. There is almost always something to be said. If there is truly nothing to be said, you can avoid the documentation.

Instead of copy-pasting Javadoc, use `@see` or other ways to link to the explanation.

An overridden function can have no Javadoc if it respects the parent's documentation with no further details.
