# BrainDot Legal

Public legal documents of BrainDot.

## Contributions

To contribute to BrainDot projects, please go to the [contrib](contrib/) directory.

## Coding Style

The coding styles followed by BrainDot can be found in the [coding-style](coding-style) directory.
